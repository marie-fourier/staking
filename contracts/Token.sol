// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract Token is ERC20 {
  address private owner;
  constructor() ERC20("Token", "TKN") {
    owner = msg.sender;
    _mint(msg.sender, 10_000_000 ether);
  }

  modifier onlyOwner {
    require(owner == msg.sender, "Only owner can use this");
    _;
  }

  function mint(address addr, uint amount) external onlyOwner {
    _mint(addr, amount);
  }

  function burn(address addr, uint amount) external onlyOwner {
    _burn(addr, amount);
  }
}

// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.11;

import "../interfaces/IERC20.sol";

/* solhint-disable not-rely-on-time */
contract Staking {
  struct Wallet {
    uint256 rewardPerToken;
    uint256 initialStakeTime;
    uint256 rewards;
    uint256 balance;
  }

  IERC20 public rewardsToken;
  IERC20 public stakingToken;

  uint256 public rewardRate = 100;
  uint256 public lastUpdateTime;
  uint256 public rewardPerTokenStored;
  mapping(address => Wallet) public wallets;

  uint256 private _totalSupply;
  uint256 private _decimals;
  address private _owner;
  uint256 private _withdrawalFee = 0; // format: 1002 = 10.02%
  uint256 private _claimLockPeriod = 60; // time in seconds
  uint256 private _stakingLockPeriod = 120; // time in seconds

  constructor(address _stakingToken, address _rewardsToken) {
    _owner = msg.sender;
    stakingToken = IERC20(_stakingToken);
    rewardsToken = IERC20(_rewardsToken);
    _decimals = rewardsToken.decimals();
  }

  // sum of rewards per 1 lp token
  function rewardPerToken() private view returns (uint256) {
    if (_totalSupply == 0) {
      return 0;
    }
    return rewardPerTokenStored +
      (((block.timestamp - lastUpdateTime) * rewardRate * (10**_decimals)) / _totalSupply);
  }

  function claimable(address account) public view returns (uint256) {
    return withFee(
      (wallets[account].balance) *
      (rewardPerToken() - wallets[account].rewardPerToken) +
      wallets[account].rewards
    );
  }

  function _updateReward(address account) internal {
    rewardPerTokenStored = rewardPerToken();
    lastUpdateTime = block.timestamp;

    wallets[account].rewards = withoutFee(claimable(account));
    wallets[account].rewardPerToken = rewardPerTokenStored;
  }

  modifier onlyOwner() {
    require(msg.sender == _owner, "Restricted operation");
    _;
  }

  function stake(uint256 _amount) external {
    _updateReward(msg.sender);
    if (wallets[msg.sender].balance == 0) {
      wallets[msg.sender].initialStakeTime = block.timestamp;
    }
    _totalSupply += _amount;
    wallets[msg.sender].balance += _amount;
    stakingToken.transferFrom(msg.sender, address(this), _amount);
  }

  function unstake(uint256 _amount) external {
    require(
      block.timestamp >= wallets[msg.sender].initialStakeTime + _stakingLockPeriod,
      "Locktime not passed"
    );
    _updateReward(msg.sender);
    _totalSupply -= _amount;
    wallets[msg.sender].balance -= _amount;
    stakingToken.transfer(msg.sender, _amount);
  }

  function claim() external {
    require(
      block.timestamp >= wallets[msg.sender].initialStakeTime + _claimLockPeriod,
      "Locktime not passed"
    );
    _updateReward(msg.sender);
    uint256 reward = withFee(wallets[msg.sender].rewards);
    wallets[msg.sender].rewards = 0;
    rewardsToken.transfer(msg.sender, reward);
  }

  function withFee(uint256 _amount) private view returns (uint256) {
    return _amount * (100 - _withdrawalFee / 100) / 100;
  }

  function withoutFee(uint256 _amount) private view returns (uint256) {
    return _amount / (100 - _withdrawalFee / 100) * 100;
  }

  function setFee(uint256 _fee) external onlyOwner {
    require(_fee <= 10000, "Invalid value");
    _withdrawalFee = _fee;
  }

  function setClaimLockPeriod(uint256 _time) external onlyOwner {
    _claimLockPeriod = _time;
  }

  function setStakingLockPeriod(uint256 _time) external onlyOwner {
    _stakingLockPeriod = _time;
  }
}

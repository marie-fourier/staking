# LP Staking

Техническое задание на неделю 2 (стейкинг)
- Написать смарт-контракт стейкинга, создать пул ликвидности на uniswap в тестовой сети. Контракт стейкинга принимает ЛП токены, после определенного времени (например 10 минут) пользователю начисляются награды в виде ревард токенов написанных на первой неделе. Количество токенов зависит от суммы застейканных ЛП токенов (например 20 процентов). Вывести застейканные ЛП токены также можно после определенного времени (например 20 минут).
- Создать пул ликвидности
- Реализовать функционал стейкинга в смарт контракте
- Написать полноценные тесты к контракту
- Написать скрипт деплоя
- Задеплоить в тестовую сеть
- Написать таски на stake, unstake, claim
- Верифицировать контракт
Требования
- Функция stake(uint256 amount) - списывает с пользователя на контракт стейкинга ЛП токены в количестве amount, обновляет в контракте баланс пользователя
- Функция claim() - списывает с контракта стейкинга ревард токены доступные в качестве наград
- Функция unstake() - списывает с контракта стейкинга ЛП токены доступные для вывода
- Функции админа для изменения параметров стейкинга (время заморозки, процент)
# Guide
- set `RINKEBY_URL` and `PRIVATE_KEY` in .env file
- Deploy ERC20 by running `npx hardhat run script/deploy-erc20.ts --network rinkeby`
- You will get `TOKEN_ADDRESS`. Set `TOKEN_ADDRESS` in .env file
- Go to https://app.uniswap.org/#/pool/v2?chain=rinkeby
- Create a pair WETH/TKN (import `TOKEN_ADDRESS`)
- Run `npx hardhat lpTokenAddress --network rinkeby` to get LP token address
- Set `LP_TOKEN_ADDRESS` in .env file
- Run `npx hardhat run scripts/deploy.ts` to deploy staking contract
- Set `CONTRACT_ADDRESS` in .env file
- Use `stake`, `unstake`, `claim`, `claimable` to interact with the staking contract
- Deployed staking contract example - https://rinkeby.etherscan.io/address/0x79abf88A186f8E2a45CA51f7Ef4E2aaF4496689A#code

# References
https://www.paradigm.xyz/2021/05/liquidity-mining-on-uniswap-v3

https://uploads-ssl.webflow.com/5ad71ffeb79acc67c8bcdaba/5ad8d1193a40977462982470_scalable-reward-distribution-paper.pdf
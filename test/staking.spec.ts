import { expect } from "chai";
import { ethers } from "hardhat";

async function getTimestamp() {
  const blockNumber = await ethers.provider.getBlockNumber();
  const block = await ethers.provider.getBlock(blockNumber);
  return block.timestamp;
}

async function increaseTime(seconds: number) {
  const timestamp = await getTimestamp();
  await setTimestamp(timestamp + seconds);
}

async function setTimestamp(timestamp: number) {
  await ethers.provider.send("evm_mine", [timestamp]);
}

function parseToken(value: string) {
  return ethers.utils.parseEther(value);
}

async function diffTimestamp(before: string) {
  return await ethers.BigNumber.from(await getTimestamp()).sub(before);
}

describe("Staking", async () => {
  let owner: any, account1: any, rewardToken: any, lpToken: any, staking: any;
  describe("One account", async () => {
    before(async () => {
      const Token = await ethers.getContractFactory("Token");
      const Staking = await ethers.getContractFactory("Staking");
      [owner, account1] = await ethers.getSigners();
      rewardToken = await Token.deploy();
      lpToken = await Token.deploy();

      await rewardToken.deployed();
      await lpToken.deployed();

      staking = await Staking.deploy(lpToken.address, rewardToken.address);

      await rewardToken.transfer(staking.address, parseToken("1000000"));
      await lpToken.transfer(account1.address, parseToken("1000"));

      await staking.deployed();
    });

    it("stake saves timestamps and transfer LP tokens", async () => {
      lpToken.approve(staking.address, parseToken("10000"));
      await expect(() =>
        staking.stake(parseToken("100"))
      ).to.changeTokenBalances(
        lpToken,
        [owner, staking],
        [parseToken("-100"), parseToken("100")]
      );
      expect((await staking.wallets(owner.address)).initialStakeTime).to.eq(
        await getTimestamp()
      );
    });

    it("should not claim and unstake during lock time", async () => {
      await expect(staking.claim()).to.be.revertedWith("Locktime not passed");
      await expect(staking.unstake(1)).to.be.revertedWith(
        "Locktime not passed"
      );
    });

    it("claimable amount should increase after some time", async () => {
      const lastUpdateTime = Number(await staking.lastUpdateTime());
      await setTimestamp(lastUpdateTime + 60);
      expect(await staking.claimable(owner.address)).to.eq(parseToken("6000"));
    });

    it("claimable amount should exclude fee", async () => {
      await staking.setFee(1000);
      const lastUpdateTime = Number(await staking.lastUpdateTime());
      await setTimestamp(lastUpdateTime + 120);
      expect(await staking.claimable(owner.address)).to.eq(parseToken("10800"));
    });

    it("claim should reset rewards and transfer tokens", async () => {
      const claimable = ethers.BigNumber.from(
        await staking.claimable(owner.address)
      ).add(parseToken("90"));
      await expect(() => {
        staking.claim();
      }).to.changeTokenBalances(rewardToken, [owner], [claimable]);
      expect((await staking.wallets(owner.address)).rewards).to.eq(0);
    });

    it("succeeding stakes should not change initial stake time", async () => {
      const timestamp = (await staking.wallets(owner.address)).initialStakeTime;
      await staking.stake(parseToken("100"));
      expect((await staking.wallets(owner.address)).initialStakeTime).to.eq(
        timestamp
      );
    });

    it("should unstake", async () => {
      await expect(() =>
        staking.unstake(parseToken("200"))
      ).to.changeTokenBalances(lpToken, [owner], [parseToken("200")]);
    });

    it("should not more than 100% fee", async () => {
      await expect(staking.setFee("100000")).to.revertedWith("Invalid value");
    });

    it("only owner can set fee and lock period", async () => {
      await staking.setFee("0");
      await expect(staking.connect(account1).setFee("0")).to.be.revertedWith(
        "Restricted operation"
      );

      await staking.setStakingLockPeriod("0");
      await expect(
        staking.connect(account1).setStakingLockPeriod("0")
      ).to.be.revertedWith("Restricted operation");

      await staking.setClaimLockPeriod("0");
      await expect(
        staking.connect(account1).setClaimLockPeriod("0")
      ).to.be.revertedWith("Restricted operation");
    });
  });

  describe("Multiple accounts", async () => {
    let stakeTimeOne: any, stakeTimeTwo: any;
    before(async () => {
      const Token = await ethers.getContractFactory("Token");
      const Staking = await ethers.getContractFactory("Staking");
      [owner, account1] = await ethers.getSigners();
      rewardToken = await Token.deploy();
      lpToken = await Token.deploy();

      await rewardToken.deployed();
      await lpToken.deployed();

      staking = await Staking.deploy(lpToken.address, rewardToken.address);

      await rewardToken.transfer(staking.address, parseToken("1000000"));
      await lpToken.transfer(account1.address, parseToken("1000"));

      await staking.deployed();
    });

    it("distributes rewards according to accounts share", async () => {
      await lpToken.approve(staking.address, parseToken("500"));
      await lpToken
        .connect(account1)
        .approve(staking.address, parseToken("1000"));

      await staking.stake(500);
      stakeTimeOne = await getTimestamp();
      await increaseTime(15);

      await staking.connect(account1).stake(1000);
      stakeTimeTwo = await getTimestamp();
      await increaseTime(15);

      expect(await staking.claimable(owner.address)).to.eq(parseToken("2100"));
      expect(await staking.claimable(account1.address)).to.eq(
        parseToken("1000")
      );
    });
  });
});

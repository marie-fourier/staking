import { expect } from "chai";
import { ethers } from "hardhat";

describe("Token", async () => {
  let owner: any, account1: any, token: any;

  before(async () => {
    const Token = await ethers.getContractFactory("Token");
    [owner, account1] = await ethers.getSigners();
    token = await Token.deploy();
    await token.deployed();
  });

  it("should return token name", async () => {
    expect(await token.name()).to.eq("Token");
  });

  it("should return token symbol", async () => {
    expect(await token.symbol()).to.eq("TKN");
  });

  it("should return token decimals", async () => {
    expect(await token.decimals()).to.eq(18);
  });

  it("should return total supply", async () => {
    expect(await token.totalSupply()).to.eq(
      ethers.utils.parseEther("10000000")
    );
  });

  it("transfer increases spenders balance and decreases senders", async () => {
    await expect(() =>
      token.transfer(account1.address, "0")
    ).to.changeTokenBalances(token, [account1, owner], ["0", "0"]);

    await expect(() =>
      token.transfer(account1.address, "1000000000")
    ).to.changeTokenBalances(
      token,
      [account1, owner],
      ["1000000000", "-1000000000"]
    );
  });

  it("should not transfer more than account has", async () => {
    await expect(
      token.transfer(account1.address, ethers.utils.parseEther("20000000"))
    ).to.be.reverted;
  });

  it("transfer emits Transfer event", async () => {
    await expect(token.transfer(account1.address, "1"))
      .to.emit(token, "Transfer")
      .withArgs(owner.address, account1.address, "1");
  });

  it("approve emits Approval event and sets spenders allowance", async () => {
    await expect(token.approve(account1.address, "10000"))
      .to.emit(token, "Approval")
      .withArgs(owner.address, account1.address, "10000");
    expect(await token.allowance(owner.address, account1.address)).to.eq(
      "10000"
    );

    await expect(token.approve(account1.address, "3000"))
      .to.emit(token, "Approval")
      .withArgs(owner.address, account1.address, "3000");
    expect(await token.allowance(owner.address, account1.address)).to.eq(
      "3000"
    );
  });

  // it("transferFrom should decrease spenders allowance and senders balance", async () => {
  //   await token.approve(account1.address, ethers.utils.parseEther("1000"));
  //   expect(await token.allowance(owner.address, account1.address)).to.eq(
  //     ethers.utils.parseEther("1000")
  //   );
  //   await expect(() =>
  //     token.transferFrom(
  //       owner.address,
  //       account1.address,
  //       ethers.utils.parseEther("7")
  //     )
  //   ).to.changeTokenBalance(token, owner, ethers.utils.parseEther("-7"));
  //   expect(await token.allowance(owner.address, account1.address)).to.eq(
  //     ethers.utils.parseEther("993")
  //   );
  // });

  it("should not transfer more than approved", async () => {
    await token.approve(account1.address, "10000");
    await expect(token.transferFrom(owner.address, account1.address, "11000"))
      .to.be.reverted;
  });

  // it("transferFrom emits Transfer event", async () => {
  //   await token.approve(account1.address, "10000");
  //   await expect(token.transferFrom(owner.address, account1.address, "3000"))
  //     .to.emit(token, "Transfer")
  //     .withArgs(owner.address, account1.address, "3000");
  // });

  it("should mint tokens and only owner can mint", async () => {
    await expect(token.connect(account1).mint(account1.address, "1")).to.be
      .reverted;

    const totalSupplyBefore = ethers.BigNumber.from(await token.totalSupply());
    await expect(() =>
      token.mint(owner.address, "300000000")
    ).to.changeTokenBalance(token, owner, "300000000");
    const totalSupplyAfter = ethers.BigNumber.from(await token.totalSupply());
    expect(totalSupplyAfter.sub(totalSupplyBefore)).to.eq("300000000");
  });

  it("should burn tokens and only owner can burn", async () => {
    await expect(token.connect(account1).burn(account1.address, "1")).to.be
      .reverted;

    const totalSupplyBefore = ethers.BigNumber.from(await token.totalSupply());
    await expect(() =>
      token.burn(owner.address, "300000000")
    ).to.changeTokenBalance(token, owner, "-300000000");
    const totalSupplyAfter = ethers.BigNumber.from(await token.totalSupply());
    expect(totalSupplyAfter.sub(totalSupplyBefore)).to.eq("-300000000");
  });
});

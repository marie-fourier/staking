import { task } from "hardhat/config";
import { getContract } from "./shared";

task("claim", "Claim rewards").setAction(async (_, hre) => {
  const staking = await getContract(hre);
  const tx = await staking.claim();
  await tx.wait();
  console.log(`Tx hash: ${tx.hash}`);
});

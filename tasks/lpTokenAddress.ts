import { task } from "hardhat/config";
import { getUniswapFactory, WETH } from "./shared";
require("dotenv").config();

task("lpTokenAddress", "Find lp token address").setAction(async (_, hre) => {
  const { TOKEN_ADDRESS } = process.env;
  if (!TOKEN_ADDRESS) {
    return;
  }
  const uniswapFactory = await getUniswapFactory(hre);
  const pair = await uniswapFactory.getPair(TOKEN_ADDRESS, WETH);
  console.log(pair);
});

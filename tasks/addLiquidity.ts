import { task } from "hardhat/config";
import {
  getAccounts,
  getTimestamp,
  getUniswapFactory,
  getUniswapRouter,
  WETH,
} from "./shared";
import { ethers } from "ethers";
require("dotenv").config();

interface TaskArgs {
  weth: string;
  token: string;
}

task("addLiquidity", "Add liquidity")
  .addParam("weth", "WETH amount")
  .addParam("token", "Tokens amount")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const { TOKEN_ADDRESS } = process.env;
    if (!TOKEN_ADDRESS) {
      return;
    }
    const UniswapRouter = await getUniswapRouter(hre);
    const ERC20 = await hre.ethers.getContractFactory("Token");
    const tokenA = await ERC20.attach(TOKEN_ADDRESS);
    const tokenB = await ERC20.attach(WETH);
    console.log("Attached to tokens");
    let tx = await tokenA.approve(
      UniswapRouter.address,
      ethers.utils.parseEther("10000000")
    );
    console.log("Approved token A");

    tx = await tokenB.approve(
      UniswapRouter.address,
      ethers.utils.parseEther("10000000")
    );
    console.log("Approved token B");

    const [owner] = await getAccounts(hre);
    const timestamp = await getTimestamp(hre);
    console.log("Timestamp:", timestamp);

    tx = await UniswapRouter.addLiquidity(
      TOKEN_ADDRESS,
      WETH,
      ethers.utils.parseEther(taskArgs.token),
      ethers.utils.parseEther(taskArgs.weth),
      1,
      1,
      owner.address,
      timestamp + 30
    );

    await tx.wait();
    console.log("Created LP");
  });

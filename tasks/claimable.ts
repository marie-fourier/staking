import { task } from "hardhat/config";
import { getAccounts, getContract } from "./shared";
import { ethers } from "ethers";

task("claimable", "Claim rewards amount").setAction(async (_, hre) => {
  const staking = await getContract(hre);
  const [owner] = await getAccounts(hre);
  const reward = await staking.claimable(owner.address);
  console.log(
    `Claimable reward amount: ${ethers.utils.formatEther(reward)} tokens`
  );
});

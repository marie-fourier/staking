import { task } from "hardhat/config";
import { ethers } from "ethers";
import { getContract } from "./shared";

interface TaskArgs {
  amount: string;
}

task("unstake", "Unstake LP tokens")
  .addParam("amount", "Unstake amount")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const staking = await getContract(hre);
    const tx = await staking.unstake(ethers.utils.parseEther(taskArgs.amount));
    await tx.wait();
    console.log(`Unstaked ${taskArgs.amount} tokens`);
  });

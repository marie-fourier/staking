require("dotenv").config();

const _decimals = 8;

export const FACTORY: string = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f";
export const ROUTER: string = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D";
export const WETH: string = "0xc778417E063141139Fce010982780140Aa0cD5Ab";

export const contractAddress = String(process.env.CONTRACT_ADDRESS);

export const getContract = async (hre: any) => {
  return await hre.ethers.getContractAt(
    "Staking",
    String(process.env.CONTRACT_ADDRESS)
  );
};

export const getAccounts = async (hre: any) => {
  return await hre.ethers.getSigners();
};

export const parseToken = (ethers: any, value: string) => {
  return ethers.BigNumber.from(value).mul(10 ** _decimals);
};

export const getTimestamp = async (hre: any) => {
  const blockNumber = await hre.ethers.provider.getBlockNumber();
  const block = await hre.ethers.provider.getBlock(blockNumber);
  return block.timestamp;
};

export const getUniswapRouter = async (hre: any) => {
  return await hre.ethers.getContractAt("IUniswapV2Router", ROUTER);
};

export const getUniswapFactory = async (hre: any) => {
  return await hre.ethers.getContractAt("IUniswapV2Factory", FACTORY);
};

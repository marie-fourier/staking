import { task } from "hardhat/config";
import { ethers } from "ethers";
import { getContract } from "./shared";
require("dotenv").config();

interface TaskArgs {
  amount: string;
}

task("stake", "Stake LP tokens")
  .addParam("amount", "Stake amount")
  .setAction(async (taskArgs: TaskArgs, hre) => {
    const staking = await getContract(hre);
    const lpToken = await hre.ethers.getContractAt(
      "Token",
      String(process.env.LP_TOKEN_ADDRESS)
    );
    await lpToken.approve(
      staking.address,
      ethers.utils.parseEther(taskArgs.amount)
    );
    const tx = await staking.stake(ethers.utils.parseEther(taskArgs.amount));
    await tx.wait();
    console.log(`Staked ${taskArgs.amount} tokens`);
  });
